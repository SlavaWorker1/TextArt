package ua.com.mitk.texttosquare;

import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import ua.com.mitk.texttosquare.models.TextArtObject;
import ua.com.mitk.texttosquare.presenters.CreationPresenter;
import ua.com.mitk.texttosquare.views.CreationView;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class CreationPresenterTest extends BaseTest {

    @Mock
    CreationView creationView;

    @Test
    public void fillModel() throws Exception {

        List<String> words = new ArrayList<>();
        words.add("green");
        words.add("yellow");
        when(creationView.getWordsTextArt()).thenReturn(words);
        when(creationView.getChoosenImageTextArt()).thenReturn(23);
        when(creationView.getColorTextArt()).thenReturn(123456);

        CreationPresenter creationPresenter = new CreationPresenter(creationView);
        creationPresenter.fillModel();

        assertEquals(TextArtObject.getTextArtObjectInstance().getColor(), 123456);
        assertEquals(TextArtObject.getTextArtObjectInstance().getText(), words);
        assertEquals(TextArtObject.getTextArtObjectInstance().getDrawableId(), 23);

    }

    @Test
    public void testSetViewPagerItem() {
        CreationPresenter creationPresenter = new CreationPresenter(creationView);

        creationPresenter.handleBackNavigation(1);
        verify(creationView).setViewPagerItem(0);
    }
}

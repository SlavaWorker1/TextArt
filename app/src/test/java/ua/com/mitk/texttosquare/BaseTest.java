package ua.com.mitk.texttosquare;

import org.junit.Before;
import org.mockito.MockitoAnnotations;



class BaseTest {

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }
}

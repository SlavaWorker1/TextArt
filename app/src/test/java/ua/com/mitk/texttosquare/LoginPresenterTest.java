package ua.com.mitk.texttosquare;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import org.junit.Test;
import org.mockito.Mock;

import ua.com.mitk.texttosquare.presenters.LoginPresenter;
import ua.com.mitk.texttosquare.views.LoginView;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class LoginPresenterTest extends BaseTest {
    @Mock
    LoginView loginView;
    @Mock
    FirebaseAuth firebaseAuth;
    @Mock
    DatabaseReference databaseReference;

    @Test
    public void checkLoginFields() {
        LoginPresenter loginPresenter = new LoginPresenter(loginView, firebaseAuth, databaseReference);

        when(loginView.getUserEmail()).thenReturn(null);
        when(loginView.getUserPassword()).thenReturn("asdaf");
        loginPresenter.onLoginClick();

        when(loginView.getUserEmail()).thenReturn("ad");
        when(loginView.getUserPassword()).thenReturn(null);
        loginPresenter.onLoginClick();

        when(loginView.getUserEmail()).thenReturn(null);
        when(loginView.getUserPassword()).thenReturn("sdfsdgs");
        loginPresenter.onLoginClick();

        verify(loginView, times(3)).showWrongFields();
    }
}

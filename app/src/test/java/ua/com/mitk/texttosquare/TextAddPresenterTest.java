package ua.com.mitk.texttosquare;

import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import ua.com.mitk.texttosquare.presenters.TextAddPresenter;
import ua.com.mitk.texttosquare.views.TextAddView;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;


public class TextAddPresenterTest extends BaseTest {

    @Mock
    TextAddView textAddView;

    @Test
    public void testGetWords() {
        String text1 = "Lorem ipsum dolor";
        String text2 = "    Lorem    ipsum dolor    ";
        String text3 = "    Lorem        ";
        String text4 = "    Lorem        ipsum";
        TextAddPresenter textAddPresenter = new TextAddPresenter(textAddView);
        List<String> wordList = new ArrayList<>();

        when(textAddView.getText()).thenReturn(text1);
        wordList.add("LOREM");
        wordList.add("IPSUM");
        wordList.add("DOLOR");
        assertEquals(wordList, textAddPresenter.getWords());
        wordList.clear();

        when(textAddView.getText()).thenReturn(text2);
        wordList.add("LOREM");
        wordList.add("IPSUM");
        wordList.add("DOLOR");
        assertEquals(wordList, textAddPresenter.getWords());
        wordList.clear();

        when(textAddView.getText()).thenReturn(text3);
        wordList.add("LOREM");;
        assertEquals(wordList, textAddPresenter.getWords());
        wordList.clear();


        when(textAddView.getText()).thenReturn(text4);
        wordList.add("LOREM");;
        wordList.add("IPSUM");;
        assertEquals(wordList, textAddPresenter.getWords());
    }

}

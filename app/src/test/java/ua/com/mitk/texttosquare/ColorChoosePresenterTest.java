package ua.com.mitk.texttosquare;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import ua.com.mitk.texttosquare.models.RecyclerViewColorItem;
import ua.com.mitk.texttosquare.presenters.ColorChoosePresenter;
import ua.com.mitk.texttosquare.views.ColorChooseView;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;




public class ColorChoosePresenterTest extends BaseTest {
    @Mock
    ColorChooseView colorChooseView;

    @Test
    public void colorClickTest() {
        List<RecyclerViewColorItem> recyclerViewColorItemList = new ArrayList<>();
        recyclerViewColorItemList.add(new RecyclerViewColorItem(1233456, false));
        recyclerViewColorItemList.add(new RecyclerViewColorItem(1324624, false));
        recyclerViewColorItemList.add(new RecyclerViewColorItem(1235665, false));
        recyclerViewColorItemList.add(new RecyclerViewColorItem(3456126, false));
        recyclerViewColorItemList.add(new RecyclerViewColorItem(1276547, false));
        recyclerViewColorItemList.add(new RecyclerViewColorItem(1281523, false));

        Mockito.when(colorChooseView.getColors()).thenReturn(recyclerViewColorItemList);

        ColorChoosePresenter colorChoosePresenter = new ColorChoosePresenter(colorChooseView);
        colorChoosePresenter.colorItemClick(5);

        verify(colorChooseView).notifyAdapter();
        verify(colorChooseView).renewColorArray(5);
        verify(colorChooseView).setGradientColors();

        assertEquals(true, recyclerViewColorItemList.get(5).isSelectedColor());


    }

}

package ua.com.mitk.texttosquare;

import org.junit.Test;

import ua.com.mitk.texttosquare.utils.StringUtils;

import static org.junit.Assert.*;


public class StringTest {
    @Test
    public void StringIsEmptyTest() throws Exception {
        assertEquals(true, StringUtils.isEmptyString(""));
        assertEquals(false, StringUtils.isEmptyString("s"));
        assertEquals(true, StringUtils.isEmptyString(" "));
        assertEquals(true, StringUtils.isEmptyString(null));
    }
}

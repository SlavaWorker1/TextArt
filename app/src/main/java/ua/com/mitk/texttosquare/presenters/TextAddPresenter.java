package ua.com.mitk.texttosquare.presenters;

import java.util.ArrayList;
import java.util.List;

import ua.com.mitk.texttosquare.utils.StringUtils;
import ua.com.mitk.texttosquare.views.TextAddView;



public class TextAddPresenter {
    private TextAddView textAddView;

    public TextAddPresenter(TextAddView textAddView) {
        this.textAddView = textAddView;
    }

    public List<String> getWords() {
        String words = textAddView.getText();
        String[] splitStr = words.split("\\s+");
        List<String> wordsList = new ArrayList<>();

        for (String aSplitStr : splitStr) {
            if (!StringUtils.isEmptyString(aSplitStr)) {
                wordsList.add(aSplitStr.toUpperCase());
            }
        }

        return wordsList;
    }
}

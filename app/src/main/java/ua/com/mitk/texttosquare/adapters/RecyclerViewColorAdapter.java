package ua.com.mitk.texttosquare.adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.models.RecyclerViewColorItem;


public class RecyclerViewColorAdapter extends RecyclerView.Adapter<RecyclerViewColorAdapter.ColorViewHolder> {


    private List<RecyclerViewColorItem> colors;
    private View borderView;
    private OnColorClick OnColorClick;


    public RecyclerViewColorAdapter(Context context) {
        this.colors = initializeColors();
        this.borderView = new View(context);
        this.borderView.setBackground(ContextCompat.getDrawable(context, R.drawable.white_border_color_tile));

    }

    public void setOnColorClick(RecyclerViewColorAdapter.OnColorClick onColorClick) {
        OnColorClick = onColorClick;
    }

    @Override
    public ColorViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_color_item, parent, false);
        return new ColorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ColorViewHolder colorViewHolder, int position) {
        colorViewHolder.linearLayout.setBackgroundColor(colors.get(position).getColor());
        colorViewHolder.linearLayout.removeAllViews();

        try {
            if (colors.get(position).isSelectedColor()) {
                if (borderView.getParent() != null) {
                    ((LinearLayout) borderView.getParent()).removeView(borderView);
                }
                if (isBlackColor(colors.get(position).getColor())) {
                    borderView.setBackground(ContextCompat.getDrawable(colorViewHolder.itemView.getContext(), R.drawable.black_border_color_tile));
                } else {
                    borderView.setBackground(ContextCompat.getDrawable(colorViewHolder.itemView.getContext(), R.drawable.white_border_color_tile));
                }
                colorViewHolder.linearLayout.addView(borderView);
            } else {
                colorViewHolder.linearLayout.removeAllViews();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public int getItemCount() {
        return colors.size();
    }

    public List<RecyclerViewColorItem> getColors() {
        return colors;
    }


    class ColorViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;

        ColorViewHolder(View itemView) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.llColor);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != -1) {
                        OnColorClick.colorClick(view, getAdapterPosition());
                    }
                }
            });
        }
    }


    private boolean isBlackColor(int color) {

        // Counting the perceptive luminance - human eye favors green color...
        double a = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;

        if (a < 0.5) {
            return true; // bright colors - black font
        } else
            return false; // dark colors - white font

    }

    private List<RecyclerViewColorItem> initializeColors() {
        // add black as first item
        List<RecyclerViewColorItem> colors = new ArrayList<>();
        colors.add(new RecyclerViewColorItem(Color.BLACK, false));

        for (int i = 0; i < 360; i += 22) {
            colors.add(new RecyclerViewColorItem(Color.HSVToColor(new float[]{i, 1f, 1f}), false));
        }

        return colors;
    }

    public View getBorderView() {
        return borderView;
    }

    public interface OnColorClick {
        void colorClick(View view, int position);
    }
}


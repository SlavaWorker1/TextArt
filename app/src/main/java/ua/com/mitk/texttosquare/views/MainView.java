package ua.com.mitk.texttosquare.views;



public interface MainView {

    void checkUserAndLogout();
    void startCreationActivity();

}

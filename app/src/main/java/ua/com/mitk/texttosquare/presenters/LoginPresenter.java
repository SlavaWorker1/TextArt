package ua.com.mitk.texttosquare.presenters;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import ua.com.mitk.texttosquare.utils.StringUtils;
import ua.com.mitk.texttosquare.views.LoginView;


public class LoginPresenter {

    private LoginView loginView;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabaseUsers;

    public LoginPresenter(final LoginView loginView, FirebaseAuth firebaseAuth, DatabaseReference mDatabaseUsers) {
        this.loginView = loginView;
        this.firebaseAuth = firebaseAuth;
        this.mDatabaseUsers = mDatabaseUsers;
        this.mDatabaseUsers.keepSynced(true);
        checkUserExist();
    }

    public void onLoginClick() {
        if (checkLoginFields()) {
            login(loginView.getUserEmail(), loginView.getUserPassword());
            loginView.showProgressDialog("Checkin Login...");
        } else {
            loginView.showWrongFields();
        }
    }

    private void login(String userEmail, String userPassword) {
        firebaseAuth.signInWithEmailAndPassword(userEmail, userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    checkUserExist();
                } else {
                    loginView.showError(task.getException().getMessage());
                }
                loginView.hideProgressDialog();
            }
        });
    }


    public void onSignUpClick() {
        loginView.signUp();
    }

    private boolean checkLoginFields() {
        String userEmail = loginView.getUserEmail();
        String userPassword = loginView.getUserPassword();

        return !(StringUtils.isEmptyString(userEmail) || StringUtils.isEmptyString(userPassword));
    }


    private void checkUserExist() {
        if (firebaseAuth.getCurrentUser() != null) {
            final String userId = firebaseAuth.getCurrentUser().getUid();
            mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(userId)) {
                        loginView.hideProgressDialog();
                        loginView.login();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

}

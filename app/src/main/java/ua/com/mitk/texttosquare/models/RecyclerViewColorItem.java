package ua.com.mitk.texttosquare.models;


public class RecyclerViewColorItem {

    private int color;
    private boolean isSelectedColor;

    public RecyclerViewColorItem(int color, boolean isSelectedColor) {
        this.color = color;
        this.isSelectedColor = isSelectedColor;
    }

    public int getColor() {
        return color;
    }

    public boolean isSelectedColor() {
        return isSelectedColor;
    }

    public void setSelectedColor(boolean selectedColor) {
        isSelectedColor = selectedColor;
    }
}


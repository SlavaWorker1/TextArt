package ua.com.mitk.texttosquare.algorithm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class WordDrawer {

    private List<Rect> rectList;
    private Context context;
    private Canvas canvas;
    private Paint mPaint;
    private Random random;
    private boolean isBold;
    private List<String> friendsNamesList;
    private List<String> friendsNamesListUntouch;
    private final float PADDING = 1f;
    private final int MIN_WIDTH_HEIGT = 3;
    private final int PERCENT_ACCURACY = 75; // Процент заполнения прямоугольника, после которого текст будет растягиваться
    private boolean isRowStartBold = true;
    private String[] boldFonts;
    private String[] thinFonts;
    private int color;
    List<Typeface> boldTypefaces, thinTypefaces;

    public WordDrawer(Bitmap imageBitmap, List<String> friendsNamesList, List<Rect> rectList, boolean isFirst, Context context, int color) {
        this.canvas = new Canvas(imageBitmap);
        this.friendsNamesList = new LinkedList<String>((friendsNamesList));
        this.friendsNamesListUntouch = new LinkedList<String>((friendsNamesList));
        this.random = new Random();
        this.rectList = rectList;
        this.context = context;
        this.color = color;

        setPaintConfig();

        boldFonts = new String[]{
                "ArimaMadurai-ExtraBold.ttf",
                "Kavoon-Regular.ttf", "Limelight-Regular.ttf", "LuckiestGuy.ttf", "Allan-Bold.ttf"
        };
        thinFonts = new String[]{
                "IndieFlower.ttf", "GloriaHallelujah.ttf", "MountainsofChristmas-Bold.ttf",
                "DancingScript-Bold.ttf", "Caveat-Bold.ttf"
        };

        boldTypefaces = new ArrayList<>();
        for (String boldFont : boldFonts) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), boldFont);
            boldTypefaces.add(typeface);
        }
        thinTypefaces = new ArrayList<>();
        for (String thinFont : thinFonts) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), thinFont);
            thinTypefaces.add(typeface);
        }

    }

    private void setPaintConfig() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setFlags(Paint.LINEAR_TEXT_FLAG);
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(color);
//        mPaint.setShader(new LinearGradient(0, 0, canvas.getWidth() / 4, canvas.getHeight() / 4, Color.BLUE, Color.RED, Shader.TileMode.MIRROR));
        mPaint.setStrokeWidth(3);
    }

    public void drawGreed(List<Rect> rectList) {
        canvas.drawColor(Color.WHITE);
        for (int i = 0; i < rectList.size(); i++) {
            canvas.drawRect(rectList.get(i), mPaint);
        }
    }

    public void drawWords() {
        canvas.drawColor(Color.WHITE);
        for (int i = 0; i < rectList.size(); i++) {
            // isRowStartBold для чередования жирности через столбец
            isRowStartBold = setBoldEven(isRowStartBold);
            isBold = isRowStartBold;
            if (!rectList.get(i).isEmpty()) {
                if (rectList.get(i).width() >= rectList.get(i).height()) {
                    drawHorizontalText(rectList.get(i), canvas);
                } else {
                    drawVerticalText(rectList.get(i), true, canvas);
                }
            }
        }
    }


    /**
     * Чередование жирного и не жирного текста
     *
     * @param isBold
     */
    private boolean setBoldEven(boolean isBold) {
        if (isBold) {
            mPaint.setTypeface(boldTypefaces.get(random.nextInt(thinTypefaces.size())));
        } else {
            mPaint.setTypeface(boldTypefaces.get(random.nextInt(boldTypefaces.size())));
        }
        return !isBold;
    }


    private void drawHorizontalText(Rect rect, Canvas canvas) {
        // isBold для чередования жирности в строке
        isBold = setBoldEven(isBold);
        boolean isCenterBorder = false;
        mPaint.setTextScaleX(1.0f);
        mPaint.setTextAlign(Paint.Align.LEFT);

        Rect bounds = new Rect();

        if (!rect.isEmpty()) {

            String bestText = getBiggestAreaWordList(rect.width(), rect.height());
            mPaint.setTextSize(setTextSizeForWidth(
                    mPaint,
                    rect.width() - PADDING,
                    rect.height() - PADDING,
                    bestText
            ));

            mPaint.getTextBounds(bestText, 0, bestText.length(), bounds);

            // сколько процентов текст занимает от длины ректангла
            int boundWidthPercentRect = (bounds.width() * 100) / rect.width();

            // если текст больше 60% (PERCENT_ACCUARY),
            // не лепим новый текст, а растягиваем старый.
            if (boundWidthPercentRect >= PERCENT_ACCURACY) {
                if (rect.width() > rect.height()) {
                    isCenterBorder = true;
                    setTextSizeForWidth(mPaint, bestText, rect.width(), rect.height());
                    mPaint.getTextBounds(bestText, 0, bestText.length(), bounds);
                }
            } else {
                mPaint.setTextScaleX(1.0f);
            }

            // Если в прямоугольник влизает только 1 текст, или это последний текст
            // выравниваем его по центру. По центру, а не по правому краю, чтоб левая часть
            // буквы тоже была более гладкая.
            if (isCenterBorder) {
                mPaint.setTextAlign(Paint.Align.CENTER);
                canvas.drawText(bestText, (rect.right + rect.left) / 2, rect.bottom + PADDING, mPaint);
            } else {
                canvas.drawText(bestText, rect.left + PADDING, rect.bottom + PADDING, mPaint);
            }
            mPaint.setTextAlign(Paint.Align.LEFT);

            mPaint.getTextBounds(bestText, 0, bestText.length(), bounds);
            int boundHeightPercentRect = (bounds.height() * 100) / rect.height();

//                 если высота текста меньше чем 70% высоты прямоугольника
            if (boundHeightPercentRect <= PERCENT_ACCURACY) {
                // создаем новый прямоугольник, из верхушки предыдущего
                Rect cropRect = new Rect();
                cropRect.left = rect.left;
                cropRect.top = rect.top;
                cropRect.right = rect.right;
                cropRect.bottom = (int) (rect.bottom - bounds.height() - PADDING);

                // доработываем конец главного прямоугольника
                rect.top = cropRect.bottom;
                rect.left = rect.left + bounds.width() + (int) PADDING;

                if (rect.width() > MIN_WIDTH_HEIGT) {
                    drawHorizontalText(rect, canvas);
                }

                if (cropRect.height() > MIN_WIDTH_HEIGT) {
                    drawHorizontalText(cropRect, canvas);
                }

                // создаем новый прямоугольник из конца предыдущего
            } else if (boundWidthPercentRect < PERCENT_ACCURACY) {
                Rect cropRect = new Rect();
                cropRect.left = rect.left + bounds.width() + (int) PADDING;
                cropRect.top = rect.top;
                cropRect.right = rect.right;
                cropRect.bottom = rect.bottom;

                if (cropRect.width() > MIN_WIDTH_HEIGT) {
                    drawHorizontalText(cropRect, canvas);
                }
            }
        }
    }


    private void drawVerticalText(Rect rect, boolean isVerticalText, Canvas canvas) {

        setBoldEven(random.nextBoolean());

        if (rect.width() >= rect.height()) {
            drawHorizontalText(rect, canvas);
            return;
        }

        if (isVerticalText) {

            Rect bounds = new Rect();
            canvas.save();
            canvas.rotate(270f, rect.right, rect.bottom);

            String bestText = getBiggestAreaWordList(rect.height(), rect.width());
            mPaint.setTextSize(setTextSizeForWidth(mPaint, rect.height(), rect.width(), bestText));

            mPaint.getTextBounds(bestText, 0, bestText.length(), bounds);

            int boundWidthPercentRect = (bounds.width() * 100) / rect.width();

            // если текст больше 70% (PERCENT_ACCUARY), не лепим а растягиваем.
            if (boundWidthPercentRect >= PERCENT_ACCURACY) {
                setTextSizeForHeight(mPaint, bestText, rect.height(), rect.width());
            } else {
                mPaint.setTextScaleX(1.0f);
            }
            mPaint.getTextBounds(bestText, 0, bestText.length(), bounds);

            mPaint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText(bestText, (rect.right + bounds.width() / 2), rect.bottom, mPaint);
            canvas.restore();
        } else {
            drawHorizontalText(rect, canvas);
        }
    }


    private String getBiggestAreaWordList(float rectWidth, float rectHeight) {

        if (friendsNamesList.size() == 1 || friendsNamesList.size() == 0) {
            friendsNamesList = new LinkedList<String>(friendsNamesListUntouch);
        }
        int area = 0;
        Rect bounds = new Rect();
        String bestWord = friendsNamesList.get(0);
        // ищет наиболее подходящие большое слово
        for (int i = 0; i < friendsNamesList.size(); i++) {
            mPaint.setTextSize(setTextSizeForWidth(mPaint, rectWidth, rectHeight, friendsNamesList.get(i)));
            mPaint.getTextBounds(friendsNamesList.get(i), 0, friendsNamesList.get(i).length(), bounds);
            int tempArea = bounds.width() * bounds.height();
            if (tempArea > area) {
                area = tempArea;
                bestWord = friendsNamesList.get(i);
                friendsNamesList.remove(bestWord);
            }
        }

        return bestWord;
    }


    private float setTextSizeForWidth(Paint paint, float desiredWidth, float desiredHeight, String text) {

        final float testTextSize = 48f;

        // Get the bounds of the text, using our testTextSize.
        paint.setTextSize(testTextSize);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);

        // Calculate the desired size as a proportion of our testTextSize.
        float desiredTextSizeHeight = testTextSize * desiredHeight / bounds.height();
        float desiredTextSizeWidth = testTextSize * desiredWidth / bounds.width();


        if (desiredTextSizeWidth >= desiredTextSizeHeight) {
            return desiredTextSizeHeight;
        } else {
            return desiredTextSizeWidth;
        }
    }

    /**
     * Растягивает текст
     */
    private void setTextSizeForWidth(Paint paint, String text, float width, float height) {

        paint.setTextSize(100);
        paint.setTextScaleX(1.0f);
        // ask the paint for the bounding rect if it were to draw this text
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        // get the height that would have been produced
        int h = bounds.bottom - bounds.top;
        // figure out what textSize setting would create that height of text
        float size = (((height - PADDING) / h) * 100f);
        // and set it into the paint
        paint.setTextSize(size);
        // Now set the scale.
        // do calculation get scale of 1.0 (no scale)
        paint.setTextScaleX(1.0f);
        // ask the paint for the bounding rect if it were to draw this text.
        paint.getTextBounds(text, 0, text.length(), bounds);
        // determine the width
        int w = bounds.right - bounds.left;
        // calculate the baseline to use so that the entire text is visible including the descenders
//        int text_h = bounds.bottom-bounds.top;
        // determine how much to scale the width to fit the view
        float xscale = ((width - PADDING)) / w;
        // set the scale for the text paint
        paint.setTextScaleX(xscale);
    }

    private void setTextSizeForHeight(Paint paint, String text, float width, float height) {

        paint.setTextSize(100);
        paint.setTextScaleX(1.0f);
        // ask the paint for the bounding rect if it were to draw this text
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        // get the height that would have been produced
        int h = bounds.bottom - bounds.top;
        // figure out what textSize setting would create that height of text
        float size = (((height) / h) * 100f);
        // and set it into the paint
        paint.setTextSize(size);
        // Now set the scale.
        // do calculation get scale of 1.0 (no scale)
        paint.setTextScaleX(1.0f);
        // ask the paint for the bounding rect if it were to draw this text.
        paint.getTextBounds(text, 0, text.length(), bounds);
        // determine the width
        int w = bounds.right - bounds.left;
        // calculate the baseline to use so that the entire text is visible including the descenders
//        int text_h = bounds.bottom-bounds.top;
        // determine how much to scale the width to fit the view
        float xscale = ((width)) / w;
        // set the scale for the text paint
        paint.setTextScaleX(xscale);
    }




}


package ua.com.mitk.texttosquare.views;


import java.util.List;

import ua.com.mitk.texttosquare.models.RecyclerViewColorItem;

public interface ColorChooseView {

    List<RecyclerViewColorItem> getColors();

    void notifyAdapter();

    void animateColorChanges(int color);

    int calcColors();

    void setGradientColors();

    void renewColorArray(int position);
}

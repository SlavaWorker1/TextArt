package ua.com.mitk.texttosquare.views;

import android.widget.ImageView;



public interface GalleryView {
    void displayImage(String uri, ImageView imgView);
    void setText(String text);
    void singleItemIntent(String postKey);
}

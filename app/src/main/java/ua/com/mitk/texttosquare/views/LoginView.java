package ua.com.mitk.texttosquare.views;


public interface LoginView {

    String getUserEmail();
    String getUserPassword();

    void showProgressDialog(String messageText);
    void hideProgressDialog();

    void login();
    void signUp();

    void showError(String errorText);

    void showWrongFields();
}

package ua.com.mitk.texttosquare.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.adapters.ImageChooserAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageChooseFragment extends Fragment {

    private RecyclerView rvTemplates;
    public ImageChooserAdapter imgChooserAdapter;
    private List<Integer> templatesList;
    public ImageView imgChoosenTemplate;


    public ImageChooseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_choose, container, false);
        rvTemplates = (RecyclerView) view.findViewById(R.id.rvTemplates);
        imgChoosenTemplate = (ImageView) view.findViewById(R.id.imgTemplate);

        fillTemplatesList(getActivity());

        imgChooserAdapter = new ImageChooserAdapter(templatesList, imgChoosenTemplate);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvTemplates.setLayoutManager(linearLayoutManager);
        rvTemplates.setAdapter(imgChooserAdapter);

        rvTemplates.post(new Runnable() {
            @Override
            public void run() {
                rvTemplates.findViewHolderForAdapterPosition(0).itemView.performClick();
            }
        });

        return view;
    }

    private void fillTemplatesList(Context context) {
        templatesList = new ArrayList<>();
        templatesList.add(R.drawable.bicycle);
        templatesList.add(R.drawable.women);
        templatesList.add(R.drawable.nosorog);
        templatesList.add(R.drawable.camera);
        templatesList.add(R.drawable.student);
    }

    public ImageChooserAdapter getImgChooserAdapter() {
        return imgChooserAdapter;
    }
}

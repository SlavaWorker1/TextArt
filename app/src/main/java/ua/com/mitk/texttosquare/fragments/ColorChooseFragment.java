package ua.com.mitk.texttosquare.fragments;


import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.adapters.RecyclerViewColorAdapter;
import ua.com.mitk.texttosquare.models.RecyclerViewColorItem;
import ua.com.mitk.texttosquare.presenters.ColorChoosePresenter;
import ua.com.mitk.texttosquare.views.ColorChooseView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ColorChooseFragment extends Fragment implements ColorChooseView {

    private final int ANIMATION_COLOR_CHANGE_DURATION = 500;
    private RecyclerView rvColors;
    private RecyclerViewColorAdapter recyclerViewColorAdapter;
    private int[] colors;
    private SeekBar seekBar;
    private GradientDrawable gradientDrawable;
    private TextView tvColorText;
    private ConstraintLayout constraintLayout;
    private ColorChoosePresenter colorChoosePresenter;

    public ColorChooseFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_color_choose, container, false);

        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "PoetsenOne-Regular.ttf");
        colors = new int[]{
                Color.WHITE,
                Color.BLACK
        };

        constraintLayout = (ConstraintLayout) view.findViewById(R.id.constrColor);
        tvColorText = (TextView) view.findViewById(R.id.tvTextColor);
        tvColorText.setTypeface(typeface);

        colorChoosePresenter = new ColorChoosePresenter(this);

        recyclerViewColorAdapter = new RecyclerViewColorAdapter(getContext());
        recyclerViewColorAdapter.getColors().get(0).setSelectedColor(true);
        recyclerViewColorAdapter.setOnColorClick(new RecyclerViewColorAdapter.OnColorClick() {
            @Override
            public void colorClick(final View view, int position) {
                colorChoosePresenter.colorItemClick(position);
            }
        });


        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvColors = (RecyclerView) view.findViewById(R.id.rvColors);
        rvColors.setLayoutManager(llm);
        rvColors.setAdapter(recyclerViewColorAdapter);


        gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
        gradientDrawable.setGradientType(GradientDrawable.LINEAR_GRADIENT);

        seekBar = (SeekBar) view.findViewById(R.id.seekBar);
        seekBar.setProgress(50);
        seekBar.setBackground(gradientDrawable);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                colorChoosePresenter.colorProgressChanged();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        constraintLayout.setBackgroundColor(calcColors());
        return view;
    }


    @Override
    public List<RecyclerViewColorItem> getColors() {
        return recyclerViewColorAdapter.getColors();
    }

    @Override
    public void notifyAdapter() {
        recyclerViewColorAdapter.notifyDataSetChanged();
    }

    @Override
    public void animateColorChanges(int resultColor) {
        ValueAnimator colorAnimation = ValueAnimator.ofObject(
                new ArgbEvaluator(),
                ((ColorDrawable) constraintLayout.getBackground()).getColor(),
                resultColor
        );
        colorAnimation.setDuration(ANIMATION_COLOR_CHANGE_DURATION); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                constraintLayout.setBackgroundColor((int) animator.getAnimatedValue());
            }

        });
        colorAnimation.start();
    }

    @Override
    public int calcColors() {
        return (int) new ArgbEvaluator().evaluate(seekBar.getProgress() / 100f, colors[0], colors[1]);
    }

    @Override
    public void setGradientColors() {
        gradientDrawable.setColors(colors);
    }

    @Override
    public void renewColorArray(int position) {
        colors[1] = recyclerViewColorAdapter.getColors().get(position).getColor();
    }

}

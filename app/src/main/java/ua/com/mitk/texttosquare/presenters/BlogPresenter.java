package ua.com.mitk.texttosquare.presenters;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ua.com.mitk.texttosquare.views.BlogView;



public class BlogPresenter {
    private DatabaseReference mDatabase;
    private BlogView blogView;

    public BlogPresenter(BlogView blogView) {
        this.blogView = blogView;
        this.mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");
        mDatabase.keepSynced(true);
    }

    public void singleItemClick(String postKey) {
        blogView.singleItemIntent(postKey);
    }

    public DatabaseReference getmDatabase() {
        return mDatabase;
    }
}

package ua.com.mitk.texttosquare.presenters;

import android.widget.ImageView;

import ua.com.mitk.texttosquare.views.GalleryView;


public class GalleryPresenter {
    private GalleryView galleryView;

    public GalleryPresenter(GalleryView galleryView) {
        this.galleryView = galleryView;
    }

    public void singleItemClick(String postKey) {
        galleryView.singleItemIntent(postKey);
    }

    public void handleImageLoaded(String uri, ImageView imageView) {
        galleryView.displayImage(uri, imageView);
    }

    public void handleUserNameLoaded(String text) {
        galleryView.setText(text);
    }

}

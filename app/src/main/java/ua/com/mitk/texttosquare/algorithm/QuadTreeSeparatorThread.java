package ua.com.mitk.texttosquare.algorithm;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

class QuadTreeSeparatorThread extends Thread {

    private QuadTreeRect quadTreeRect;
    private int number;
    private Bitmap bitmap;
    private int minSquareSize;
    private List<Rect> splitedRect;
    private int[][] pixelsBitmap;

    QuadTreeSeparatorThread(QuadTreeRect quadTreeRect, int minSquareSize, Bitmap bitmap, int number) {
        this.quadTreeRect = quadTreeRect;
        this.number = number;
        this.bitmap = bitmap;
        this.minSquareSize = minSquareSize;
        fillPixels();
    }

    @Override
    public void run() {
        splitedRect = new ArrayList<>();
//        Log.d("ROME", "run: " + number + "start");
        goInsindeTree(quadTreeRect);
//        Log.d("ROME", "run: " + number + "end");
    }

    private void goInsindeTree(QuadTreeRect quadTreeRect) {
        if (quadTreeRect.getGraphicRect().width() < minSquareSize) return;
        double occupancyOfRectangle = checkQuadFill(quadTreeRect.getGraphicRect());
        if (occupancyOfRectangle >= 0.99) {
            splitedRect.add(quadTreeRect.getGraphicRect());
        } else {
            if (occupancyOfRectangle >= 0.005 && occupancyOfRectangle < 0.99) {
                goInsindeTree(quadTreeRect.getLeftBottom());
                goInsindeTree(quadTreeRect.getLeftTop());
                goInsindeTree(quadTreeRect.getRightBottom());
                goInsindeTree(quadTreeRect.getRightTop());
            }
        }
    }


    private double checkQuadFill(Rect rect) {
        int whitePixels = 0;
        int blackPixels = 0;
        int step = 1;
//        int step = rect.width() / 50;
//        if (step < 1) step = 1;

        for (int i = rect.left; i < rect.right; i += step) {
            for (int j = rect.top; j < rect.bottom; j += step) {
                if (checkPixelColor(
                        pixelsBitmap[i][j]
                )) {
                    blackPixels++;
                } else {
                    whitePixels++;
                }
            }
        }
        return ((double) blackPixels) / ((double) (whitePixels + blackPixels));
    }


    /**
     * @return Принадлежит ли пиксель заданному цветовому диапазону
     */
    private boolean checkPixelColor(int pixel) {
        int MIN_PIXEL_COLOR = 85;
        return (Color.red(pixel) <= MIN_PIXEL_COLOR && Color.green(pixel) <= MIN_PIXEL_COLOR && Color.blue(pixel) <= MIN_PIXEL_COLOR);
//        return pixel == Color.BLACK;
    }

    List<Rect> getSplitedRect() {
        return splitedRect;
    }

    private void fillPixels() {
        int[] imagePixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(imagePixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        pixelsBitmap = new int[bitmap.getWidth()][bitmap.getHeight()];
        for (int i = 0; i < bitmap.getWidth(); i++)
            for (int j = 0; j < bitmap.getHeight(); j++)
                pixelsBitmap[i][j] = imagePixels[(j * bitmap.getWidth()) + i];
    }
}

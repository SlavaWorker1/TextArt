package ua.com.mitk.texttosquare.views;


import java.util.List;

public interface CreationView {

    int getChoosenImageTextArt();
    int getColorTextArt();
    List<String> getWordsTextArt();
    void setMenuItemChecked(int position, boolean isChecked);
    void finishActivity();
    void setViewPagerItem(int itemNumb);
}

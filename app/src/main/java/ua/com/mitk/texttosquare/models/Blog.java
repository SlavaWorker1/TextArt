package ua.com.mitk.texttosquare.models;


public class Blog {

    private String image;
    private String username;

    public Blog() {
    }

    public Blog(String image, String username) {
        this.image = image;
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

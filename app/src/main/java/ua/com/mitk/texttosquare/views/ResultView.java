package ua.com.mitk.texttosquare.views;

import android.net.Uri;

public interface ResultView {

    void showCreatingDialog();
    void showUploadingDialog();
    void hideProgressDialog();
    Uri getImageUri();
    void drawWords();
    void showError();
    void setEmptyBitmap();
    void createShare();
}

package ua.com.mitk.texttosquare;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

//View Holder for RecyclerView
public class BlogViewHolder extends RecyclerView.ViewHolder {

    public View mView;
    //        TextView postTitle;
    // For like post


    public BlogViewHolder(View itemView) {
        super(itemView);
        mView = itemView;

            /*
            // For clicking single post title, we did it generate
            postTitle = (TextView) mView.findViewById(R.id.post_title);

            postTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            */

    }

    public void setUsername(String username) {
        TextView postUsername = (TextView) mView.findViewById(R.id.post_username);
        postUsername.setText("created by: " + username);
    }

    public void setImage(String image) {
        ImageView postImage = (ImageView) mView.findViewById(R.id.post_image);
        ImageLoader.getInstance().displayImage(image, postImage);

    }
}
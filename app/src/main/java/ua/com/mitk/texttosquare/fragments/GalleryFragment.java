package ua.com.mitk.texttosquare.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.nostra13.universalimageloader.core.ImageLoader;

import de.hdodenhof.circleimageview.CircleImageView;
import ua.com.mitk.texttosquare.BlogViewHolder;
import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.activities.BlogSingleActivity;
import ua.com.mitk.texttosquare.models.Blog;
import ua.com.mitk.texttosquare.presenters.GalleryPresenter;
import ua.com.mitk.texttosquare.views.GalleryView;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment implements GalleryView {

    private RecyclerView rvGallery;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseUser;
    private Query curentUserPosts;
    private CircleImageView imgUserAvatar;
    private final String TAG = GalleryFragment.class.getSimpleName();
    private TextView tvUserName;
    private GalleryPresenter gallertPresenter;


    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);

        gallertPresenter = new GalleryPresenter(this);

        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUserId);
        mDatabase.keepSynced(true);

        curentUserPosts = mDatabase.orderByChild("uid").equalTo(currentUserId);
        tvUserName = (TextView) view.findViewById(R.id.tvUsername);

        imgUserAvatar = (CircleImageView) view.findViewById(R.id.imgAvatar);

        mDatabaseUser.child("image").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    gallertPresenter.handleImageLoaded(dataSnapshot.getValue().toString(), imgUserAvatar);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabaseUser.child("name").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                gallertPresenter.handleUserNameLoaded(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        rvGallery = (RecyclerView) view.findViewById(R.id.rvGallery);
        rvGallery.setHasFixedSize(true);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Blog, BlogViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(
                Blog.class,
                R.layout.blog_item,
                BlogViewHolder.class,
                curentUserPosts
        ) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {
                final String postKey = getRef(position).getKey();

                viewHolder.setImage(model.getImage());
                viewHolder.setUsername(model.getUsername());

                // For recyclerview item click

                // Reference of our views
                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gallertPresenter.singleItemClick(postKey);
                    }
                });

            }
        };
        rvGallery.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.notifyDataSetChanged();
        RecyclerView.LayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        rvGallery.setLayoutManager(layoutManager);

    }

    @Override
    public void displayImage(String uri, ImageView imgView) {
        ImageLoader.getInstance().displayImage(uri, imgView);
    }

    @Override
    public void setText(String text) {
        tvUserName.setText(text);

    }

    @Override
    public void singleItemIntent(String postKey) {
        Intent singleBlogItem = new Intent(getActivity(), BlogSingleActivity.class);
        singleBlogItem.putExtra("postId", postKey);
        startActivity(singleBlogItem);
    }
}

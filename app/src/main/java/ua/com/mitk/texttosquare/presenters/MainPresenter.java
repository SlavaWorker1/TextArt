package ua.com.mitk.texttosquare.presenters;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ua.com.mitk.texttosquare.views.MainView;



public class MainPresenter {

    private DatabaseReference mDatabaseUsers;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private MainView mainView;
    public MainPresenter(final MainView mainView) {
        this.mainView = mainView;
        this.mAuth = FirebaseAuth.getInstance();
        this.mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (!isUserLoginIn(firebaseAuth)) {
                    mainView.checkUserAndLogout();
                }
            }
        };
        this.mAuth.addAuthStateListener(mAuthListener);
        this.mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        this.mDatabaseUsers.keepSynced(true);
    }
    public void onFloatingActionButtonClick() {
        mainView.startCreationActivity();
    }
    private boolean isUserLoginIn(FirebaseAuth firebaseAuth) {
        return firebaseAuth.getCurrentUser() != null;
    }

    public void signOut() {
        mAuth.signOut();
    }
}




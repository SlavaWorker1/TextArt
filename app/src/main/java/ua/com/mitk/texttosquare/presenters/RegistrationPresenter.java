package ua.com.mitk.texttosquare.presenters;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.yalantis.ucrop.UCrop;

import java.util.Random;

import ua.com.mitk.texttosquare.utils.StringUtils;
import ua.com.mitk.texttosquare.views.RegistrationView;


public class RegistrationPresenter {

    private final static int REQUEST_IMAGE = 200;
    private final static  int RESULT_OK = -1;

    private FirebaseAuth mAuth;
    private RegistrationView registrationView;
    private StorageReference mStorageImage;
    private DatabaseReference mDatabaseUsers;


    public RegistrationPresenter(RegistrationView registrationView) {
        this.registrationView = registrationView;
        this.mAuth = FirebaseAuth.getInstance();
        this.mStorageImage = FirebaseStorage.getInstance().getReference().child("Profile_images");
        this.mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    public void onAvatarClick() {
        registrationView.startImagePickActivity(200);

    }

    public void onRegistrationClick() {
        if (checkSetUpFields()) {
            registrationView.showProgressDialog("Registration...");
            mAuth.createUserWithEmailAndPassword(
                    registrationView.getUserEmail(),
                    registrationView.getUserPassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        //TODO add fields
                        final String userId = mAuth.getCurrentUser().getUid();
                        final DatabaseReference curentUserDb = mDatabaseUsers.child(userId);

                        curentUserDb.child("name").setValue(registrationView.getUserName());

                        StorageReference filePath = mStorageImage.child(new Random().nextInt(1000) + registrationView.getUserAvatarUri().getLastPathSegment());
                        filePath.putFile(registrationView.getUserAvatarUri()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                @SuppressWarnings("VisibleForTests")
                                String downloadUri = taskSnapshot.getDownloadUrl().toString();
                                curentUserDb.child("image").setValue(downloadUri);
                                registrationView.setUpAccount();
                            }
                        });
                    } else {
                        registrationView.showError(task.getException().getMessage());
                    }
                    registrationView.hideProgressDialog();

                }
            });
        } else {
            registrationView.showError("Please fill all fields");
        }
    }

    public void onResult(int requestCode, int resultCode) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                registrationView.createUri();
            }
        }
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            registrationView.setUserAvatar();
        }
    }


    private boolean checkSetUpFields() {
        String userName = registrationView.getUserName();
        String userEmail = registrationView.getUserEmail();
        String userPassword = registrationView.getUserPassword();
        return !StringUtils.isEmptyString(userName)
                && !StringUtils.isEmptyString(userEmail)
                && !StringUtils.isEmptyString(userPassword)
                && registrationView.getUserAvatarUri() != null;
    }


}

package ua.com.mitk.texttosquare.algorithm;


import android.graphics.Rect;

import java.util.List;

class QuadrantsCombinerThread extends Thread {

    private List<Rect> rectsList;

    QuadrantsCombinerThread(List<Rect> rectsList) {
        this.rectsList = rectsList;
    }

    @Override
    public void run() {
        int unionCount = createHorizontalUnion(rectsList) + createVerticalUnion(rectsList);
        while (unionCount > 0) {
            unionCount = createHorizontalUnion(rectsList) + createVerticalUnion(rectsList);
        }
    }


    /**
     * Объединяет прямоугольники имеющие одинаковые left и right
     * возвращает изменения если полученный прямоугольник имеет width > height
     *
     * @param rectList
     * @return
     */
    private int createVerticalUnion(List<Rect> rectList) {
        int unionCount = 0;
        for (int i = 0; i < rectList.size(); i++) {
            if (!rectList.get(i).isEmpty()) {
                for (int j = 0; j < i; j++) {
                    if (rectList.get(i).bottom == rectList.get(j).top &&
                            (rectList.get(i).left == rectList.get(j).left &&
                                    rectList.get(i).right == rectList.get(j).right)) {
                        rectList.get(i).bottom = rectList.get(j).bottom;
                        rectList.get(j).setEmpty();
                        unionCount++;
                    }
                }
            }
        }
        return unionCount;
    }


    private int createHorizontalUnion(List<Rect> rectList) {
        int unionCount = 0;
        for (int i = 0; i < rectList.size(); i++) {
            if (!rectList.get(i).isEmpty()) {
                for (int j = i; j < rectList.size(); j++) {
                    if (rectList.get(i).right == rectList.get(j).left &&
                            (rectList.get(i).top == rectList.get(j).top &&
                                    rectList.get(i).bottom == rectList.get(j).bottom)) {
                        rectList.get(i).right = rectList.get(j).right;
                        rectList.get(j).setEmpty();
                        unionCount++;
                    }
                }
            }
        }
        return unionCount;
    }

    public List<Rect> getRectsList() {
        return rectsList;
    }
}

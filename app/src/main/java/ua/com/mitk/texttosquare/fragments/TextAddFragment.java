package ua.com.mitk.texttosquare.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.presenters.TextAddPresenter;
import ua.com.mitk.texttosquare.views.TextAddView;

/**
 * A simple {@link Fragment} subclass.
 */
public class TextAddFragment extends Fragment implements TextAddView {

    private EditText edText;
    private TextAddPresenter textAddPresenter;


    public TextAddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_text_add, container, false);
        textAddPresenter = new TextAddPresenter(this);
        edText = (EditText) view.findViewById(R.id.editText);
        return view;
    }

    public TextAddPresenter getTextAddPresenter() {
        return textAddPresenter;
    }

    @Override
    public String getText() {
        return edText.getText().toString();
    }
}

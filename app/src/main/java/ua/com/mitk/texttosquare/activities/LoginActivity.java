package ua.com.mitk.texttosquare.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.presenters.LoginPresenter;
import ua.com.mitk.texttosquare.views.LoginView;

public class LoginActivity extends FirebaseActivity implements LoginView {

    private EditText edEmail, edPassword;
    private ProgressDialog progressDialog;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        super.onCreate(savedInstanceState);
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatabaseUsers.keepSynced(true);

        loginPresenter = new LoginPresenter(this, firebaseAuth, mDatabaseUsers);
        setContentView(R.layout.activity_login);


        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        edEmail = (EditText) findViewById(R.id.edEmail);
        edPassword = (EditText) findViewById(R.id.edPassword);
        progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);

        TextView tvLogo = (TextView) findViewById(R.id.tvLogo);
        tvLogo.setTypeface(Typeface.createFromAsset(this.getAssets(), "PoetsenOne-Regular.ttf"));

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginPresenter.onLoginClick();
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginPresenter.onSignUpClick();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public String getUserEmail() {
        return edEmail.getText().toString();
    }

    @Override
    public String getUserPassword() {
        return edPassword.getText().toString();
    }

    @Override
    public void showProgressDialog(String messageText) {
        progressDialog.setMessage(messageText);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    public void login() {
        Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }


    @Override
    public void signUp() {
        startActivity(new Intent(LoginActivity.this, RegistrationActivity.class));
    }

    @Override
    public void showError(String errorText) {
        Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWrongFields() {
        Toast.makeText(getApplicationContext(), "Wrong fields", Toast.LENGTH_SHORT).show();
    }
}

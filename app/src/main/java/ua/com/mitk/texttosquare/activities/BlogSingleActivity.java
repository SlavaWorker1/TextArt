package ua.com.mitk.texttosquare.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.presenters.BlogSingleItemPresenter;
import ua.com.mitk.texttosquare.views.SingleBlogItemView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class BlogSingleActivity extends AppCompatActivity implements SingleBlogItemView {

    private PhotoViewAttacher mAttacher;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_single);

        BlogSingleItemPresenter blogSingleItemPresenter = new BlogSingleItemPresenter(
                this,
                getIntent().getExtras().getString("postId")
        );

        mImageView = (ImageView) findViewById(R.id.imgResult);

    }

    @Override
    public void setPhotoAttacher(String uri) {
        ImageLoader.getInstance().displayImage(uri, mImageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                mAttacher = new PhotoViewAttacher(mImageView);
                mAttacher.setMaximumScale(5.0f);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }
}

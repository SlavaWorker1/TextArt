package ua.com.mitk.texttosquare.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.algorithm.InitialGrid;
import ua.com.mitk.texttosquare.algorithm.WordDrawer;
import ua.com.mitk.texttosquare.models.TextArtObject;
import ua.com.mitk.texttosquare.presenters.ResultPresenter;
import ua.com.mitk.texttosquare.utils.ImageDecoder;
import ua.com.mitk.texttosquare.views.ResultView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends Fragment implements ResultView {

    private ImageView imgResult;
    private Bitmap mutableBitmap;
    private Button btnUpload;
    private Button btnShare;
    private ProgressDialog progressDialog;
    private TextArtObject textArtObject;
    private static Handler handler;
    private ResultPresenter resultPresenter;
    private Uri uri;


    public ResultFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result, container, false);

        resultPresenter = new ResultPresenter(this);

        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);

        imgResult = (ImageView) view.findViewById(R.id.imgResult);
        btnUpload = (Button) view.findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startPosting();
                resultPresenter.startPosting();
            }
        });
        btnShare = (Button) view.findViewById(R.id.btnShare);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultPresenter.share();
            }
        });


        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                // обновляем ImageView
                imgResult.setImageBitmap(mutableBitmap);
                hideProgressDialog();
            }
        };


        return view;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        resultPresenter = new ResultPresenter(this);
        resultPresenter.onFragmentVisible(isVisibleToUser);
    }


    @Override
    public void showCreatingDialog() {
        progressDialog.setMessage("Creating");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void showUploadingDialog() {
        progressDialog.setMessage("Uploading");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    public Uri getImageUri() {
        return uri;
    }

    @Override
    public void drawWords() {
        textArtObject = TextArtObject.getTextArtObjectInstance();
        final Bitmap bm = BitmapFactory.decodeResource(getResources(), textArtObject.getDrawableId());
        mutableBitmap = bm.copy(Bitmap.Config.ARGB_8888, true);
        int minimumQuade = (int) (Math.sqrt(mutableBitmap.getWidth() * mutableBitmap.getHeight())) / 500;
        InitialGrid initialGrid = new InitialGrid(mutableBitmap, minimumQuade);
        final WordDrawer wordDrawer = new WordDrawer(
                mutableBitmap,
                textArtObject.getText(),
                initialGrid.getSplitedRect(),
                true,
                getActivity(),
                textArtObject.getColor()
        );
        wordDrawer.drawWords();
        uri = getImageUri(getActivity(), mutableBitmap);
        handler.sendEmptyMessage(1);
    }

    @Override
    public void showError() {
        Toast.makeText(getActivity(), "You not entered text", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setEmptyBitmap() {
        imgResult.setImageBitmap(null);
    }

    @Override
    public void createShare() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpg");
        File file = new File(ImageDecoder.writeBitmapToFile(getActivity(), mutableBitmap));
        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        startActivity(Intent.createChooser(share, "Share"));
    }
}

package ua.com.mitk.texttosquare.utils;


public class StringUtils {

    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().length() <= 0);
    }
}

package ua.com.mitk.texttosquare.algorithm;

import android.graphics.Rect;


class QuadTreeRect {

    private Rect mainRect;
    private QuadTreeRect leftTop;
    private QuadTreeRect rightTop;
    private QuadTreeRect leftBottom;
    private QuadTreeRect rightBottom;

    QuadTreeRect(Rect mainRect) {
        this.mainRect = mainRect;
    }

    private QuadTreeRect(int left, int top, int right, int bottom) {
        this.mainRect = new Rect(left, top, right, bottom);
    }

    private void createDescendants() {
        leftTop = new QuadTreeRect(mainRect.left, mainRect.top, mainRect.centerX(), mainRect.centerY());
        rightTop = new QuadTreeRect(mainRect.centerX(), mainRect.top, mainRect.right, mainRect.centerY());
        leftBottom = new QuadTreeRect(mainRect.left, mainRect.centerY(), mainRect.centerX(), mainRect.bottom);
        rightBottom = new QuadTreeRect(mainRect.centerX(), mainRect.centerY(), mainRect.right, mainRect.bottom);

    }

    Rect getGraphicRect() {
        return mainRect;
    }

    QuadTreeRect getLeftTop() {
        if (leftTop == null) createDescendants();
        return leftTop;
    }

    QuadTreeRect getRightTop() {
        if (rightTop == null) createDescendants();
        return rightTop;
    }

    QuadTreeRect getLeftBottom() {
        if (leftBottom == null) createDescendants();
        return leftBottom;
    }

    QuadTreeRect getRightBottom() {
        if (rightBottom == null) createDescendants();
        return rightBottom;
    }
}

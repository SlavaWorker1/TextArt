package ua.com.mitk.texttosquare.presenters;


import ua.com.mitk.texttosquare.models.TextArtObject;
import ua.com.mitk.texttosquare.views.CreationView;

public class CreationPresenter {


    private CreationView creationView;
    private TextArtObject textArtObject;

    public CreationPresenter(CreationView creationView) {
        this.creationView = creationView;
        this.textArtObject = TextArtObject.getTextArtObjectInstance();
    }

    public void fillModel() {
        textArtObject.setDrawableId(creationView.getChoosenImageTextArt());
        textArtObject.setText(creationView.getWordsTextArt());
        textArtObject.setColor(creationView.getColorTextArt());
    }

    public void handleBackNavigation(int position) {
        if (position != 0) {
            creationView.setViewPagerItem(position - 1);
        } else {
            creationView.finishActivity();
        }
    }

    public void handleMenuClick(int position) {
        creationView.setMenuItemChecked(
                position,
                true
        );
    }

}

package ua.com.mitk.texttosquare.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.yalantis.ucrop.UCrop;

import java.io.File;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.presenters.RegistrationPresenter;
import ua.com.mitk.texttosquare.utils.ImagePicker;
import ua.com.mitk.texttosquare.views.RegistrationView;

public class RegistrationActivity extends AppCompatActivity  implements RegistrationView {

    private EditText edName, edEmail, edPassword;
    private Button btnReg;
    private ProgressDialog mProgress;
    private ImageView imgAvatar;
    private Uri mImageUri = null;
    private ImagePicker imagePicker;
    private Intent intentData;
    private RegistrationPresenter registrationPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mProgress = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);

        imagePicker = new ImagePicker(this);
        registrationPresenter = new RegistrationPresenter(this);

        edName = (EditText) findViewById(R.id.edName);
        edEmail = (EditText) findViewById(R.id.edEmail);
        edPassword = (EditText) findViewById(R.id.edPassword);

        btnReg = (Button) findViewById(R.id.btnReg);
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrationPresenter.onRegistrationClick();
            }
        });


        imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        imgAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrationPresenter.onAvatarClick();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        intentData = data;
        registrationPresenter.onResult(requestCode, resultCode);
    }


    @Override
    public void setUpAccount() {
        Intent mainIntent = new Intent(RegistrationActivity.this, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
    }

    @Override
    public void setUserAvatar() {
        mImageUri = UCrop.getOutput(intentData);
        Bitmap bitmap = BitmapFactory.decodeFile(mImageUri.getPath());
        imgAvatar.setImageBitmap(bitmap);
    }

    @Override
    public String getUserName() {
        return edName.getText().toString();
    }

    @Override
    public String getUserEmail() {
        return edEmail.getText().toString();
    }

    @Override
    public String getUserPassword() {
        return edPassword.getText().toString();
    }

    @Override
    public void showProgressDialog(String progressText) {
        mProgress.setMessage(progressText);
        mProgress.show();

    }

    @Override
    public void hideProgressDialog() {
        mProgress.dismiss();
    }

    @Override
    public void setUpUCropActivity(Uri imageUri, Uri destinationUri) {
        UCrop.Options options = new UCrop.Options();
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorAccent));
        UCrop.of(imageUri, destinationUri)
                .withAspectRatio(1, 1)
                .withMaxResultSize(500, 500)
                .withOptions(options)
                .start(this);
    }

    @Override
    public void startImagePickActivity(int code) {
        startActivityForResult(imagePicker.getPickImageChooserIntent(), code);
    }

    @Override
    public void createUri() {
        Uri imageUri = imagePicker.getPickImageResultUri(intentData);
        Uri destinationUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "sh_avatar.jpg"));
        setUpUCropActivity(imageUri, destinationUri);

    }

    @Override
    public void showError(String errorText) {
        Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Uri getUserAvatarUri() {
        return mImageUri;
    }

}

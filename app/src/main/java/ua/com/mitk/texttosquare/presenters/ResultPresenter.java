package ua.com.mitk.texttosquare.presenters;


import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import ua.com.mitk.texttosquare.models.TextArtObject;
import ua.com.mitk.texttosquare.views.ResultView;

public class ResultPresenter {
    private ResultView resultView;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;
    private TextArtObject textArtObject;
    private DatabaseReference mDatabaseUser;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;


    public ResultPresenter(ResultView resultView) {
        this.resultView = resultView;
        this.mStorage = FirebaseStorage.getInstance().getReference();
        this.mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");
        this.textArtObject = TextArtObject.getTextArtObjectInstance();
        this.mAuth = FirebaseAuth.getInstance();
        this.mCurrentUser = mAuth.getCurrentUser();
        this.mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());
    }


    public void startPosting() {
        resultView.showUploadingDialog();

        Log.d("ResultPresenter", "startPosting: " + resultView.getImageUri());
        StorageReference filePath = mStorage.child("Blog_images").child(resultView.getImageUri().getLastPathSegment());
        filePath.putFile(resultView.getImageUri()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests")
                final Uri downloadUri = taskSnapshot.getDownloadUrl();

                final DatabaseReference newPost = mDatabase.push();
                mDatabaseUser.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        newPost.child("image").setValue(downloadUri.toString());
                        newPost.child("uid").setValue(mCurrentUser.getUid());
                        newPost.child("username").setValue(dataSnapshot.child("name").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                resultView.showError();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                resultView.hideProgressDialog();

            }

        });
    }

    public void onFragmentVisible(boolean isFragmentVisible) {
        if (isFragmentVisible) {
            if (textArtObject.getText().size() > 0) {
                resultView.setEmptyBitmap();
                resultView.showCreatingDialog();
                Thread t = new Thread(new Runnable() {
                    public void run() {
                        resultView.drawWords();
                    }
                });
                t.start();
            } else {
                //TODO handle error
                resultView.showError();
            }
        }
    }

    public void share() {
        resultView.createShare();
    }
}


package ua.com.mitk.texttosquare.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.adapters.SectionsPagerAdapter;
import ua.com.mitk.texttosquare.fragments.BlogFragment;
import ua.com.mitk.texttosquare.fragments.GalleryFragment;
import ua.com.mitk.texttosquare.presenters.MainPresenter;
import ua.com.mitk.texttosquare.views.MainView;

public class MainActivity extends AppCompatActivity implements MainView {

    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("TextArt your imagination !");
        setSupportActionBar(toolbar);

        mainPresenter = new MainPresenter(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainPresenter.onFloatingActionButtonClick();

            }
        });

        ViewPager mViewPager = (ViewPager) findViewById(R.id.container);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

    }

    private void setupViewPager(ViewPager mViewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        GalleryFragment galleryFragment = new GalleryFragment();
        BlogFragment blogFragment = new BlogFragment();
        adapter.addFragment(galleryFragment);
        adapter.addFragment(blogFragment);
        mViewPager.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.menu_log_out:
                mainPresenter.signOut();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void checkUserAndLogout() {
        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

    @Override
    public void startCreationActivity() {
        startActivity(new Intent(MainActivity.this, CreationActivity.class));
    }
}

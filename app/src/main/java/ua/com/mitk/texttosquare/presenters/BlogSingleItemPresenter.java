package ua.com.mitk.texttosquare.presenters;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ua.com.mitk.texttosquare.views.SingleBlogItemView;



public class BlogSingleItemPresenter {

    public BlogSingleItemPresenter(final SingleBlogItemView singleBlogItemView, String postKey) {

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Blog");
        mDatabase.child(postKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String postImage = (String) dataSnapshot.child("image").getValue();
                singleBlogItemView.setPhotoAttacher(postImage);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}

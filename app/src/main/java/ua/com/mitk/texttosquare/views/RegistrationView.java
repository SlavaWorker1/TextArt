package ua.com.mitk.texttosquare.views;

import android.net.Uri;


public interface RegistrationView {

    void setUpAccount();
    void setUserAvatar();

    String getUserName();
    String getUserEmail();
    String getUserPassword();

    void showProgressDialog(String progressText);
    void hideProgressDialog();

    void setUpUCropActivity(Uri imageUri, Uri destinationUri);
    void startImagePickActivity(int code);

    void createUri();
    void showError(String errorText);

    Uri getUserAvatarUri();

}

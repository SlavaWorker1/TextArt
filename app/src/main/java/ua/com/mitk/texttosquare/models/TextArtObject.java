package ua.com.mitk.texttosquare.models;

import java.util.List;


public class TextArtObject {
    private static TextArtObject textArtObject;
    private int drawableId;
    private int color;
    private List<String> text;

    private TextArtObject() {

    }

    public void setDrawableId(int drawableId) {
        textArtObject.drawableId = drawableId;
    }

    public void setColor(int color) {
        textArtObject.color = color;
    }

    public void setText(List<String> text) {
        textArtObject.text = text;
    }

    public int getDrawableId() {
        return textArtObject.drawableId;
    }

    public int getColor() {
        return textArtObject.color;
    }

    public List<String> getText() {
        return textArtObject.text;
    }

    public static TextArtObject getTextArtObjectInstance() {
        if (textArtObject == null) {
            synchronized (TextArtObject.class) {
                if (textArtObject == null)
                    textArtObject = new TextArtObject();
            }
        }
        return textArtObject;
    }
}

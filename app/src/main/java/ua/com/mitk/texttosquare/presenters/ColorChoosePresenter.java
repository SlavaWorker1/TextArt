package ua.com.mitk.texttosquare.presenters;

import ua.com.mitk.texttosquare.views.ColorChooseView;


public class ColorChoosePresenter {
    private ColorChooseView colorChooseView;

    public ColorChoosePresenter(ColorChooseView colorChooseView) {
        this.colorChooseView = colorChooseView;

    }

    public void colorItemClick(int position) {
        for (int i = 0; i < colorChooseView.getColors().size(); i++) {
            colorChooseView.getColors().get(i).setSelectedColor(false);
            colorChooseView.getColors().get(position).setSelectedColor(true);
        }
        colorChooseView.renewColorArray(position);
        colorChooseView.setGradientColors();
        colorChooseView.animateColorChanges(colorChooseView.calcColors());
        colorChooseView.notifyAdapter();
    }

    public void colorProgressChanged() {
        colorChooseView.animateColorChanges(colorChooseView.calcColors());
    }

}

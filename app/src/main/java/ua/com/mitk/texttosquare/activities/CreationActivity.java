package ua.com.mitk.texttosquare.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.List;

import ua.com.mitk.texttosquare.BlockedViewPager;
import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.adapters.ViewPagerAdapter;
import ua.com.mitk.texttosquare.fragments.ColorChooseFragment;
import ua.com.mitk.texttosquare.fragments.ImageChooseFragment;
import ua.com.mitk.texttosquare.fragments.ResultFragment;
import ua.com.mitk.texttosquare.fragments.TextAddFragment;
import ua.com.mitk.texttosquare.presenters.CreationPresenter;
import ua.com.mitk.texttosquare.views.CreationView;

public class CreationActivity extends AppCompatActivity implements CreationView {

    private BlockedViewPager viewPager;
    private BottomNavigationView navigation;
    private ViewPagerAdapter adapter;
    private CreationPresenter creationPresenter;
    private ImageChooseFragment imageChooseFragment;
    private ColorChooseFragment colorChooseFragment;
    private TextAddFragment textAddFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation);

        try {
            getSupportActionBar().setTitle("Create your TextArt!");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        creationPresenter = new CreationPresenter(this);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        viewPager = (BlockedViewPager) findViewById(R.id.viewpager);
        viewPager.setPagingEnabled(false);
        setupViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                creationPresenter.handleMenuClick(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_gallery:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_color:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_text:
                    viewPager.setCurrentItem(2);
                    return true;
                case R.id.navigation_create:
                    creationPresenter.fillModel();
                    viewPager.setCurrentItem(3);
                    return true;
            }
            return false;
        }

    };

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setOffscreenPageLimit(3);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        imageChooseFragment = new ImageChooseFragment();
        colorChooseFragment = new ColorChooseFragment();
        textAddFragment = new TextAddFragment();
        ResultFragment resultFragment = new ResultFragment();
        adapter.addFragment(imageChooseFragment);
        adapter.addFragment(colorChooseFragment);
        adapter.addFragment(textAddFragment);
        adapter.addFragment(resultFragment);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        creationPresenter.handleBackNavigation(viewPager.getCurrentItem());
    }

    @Override
    public int getChoosenImageTextArt() {
        return imageChooseFragment.getImgChooserAdapter().getDrawableId();
    }

    @Override
    public int getColorTextArt() {
        return colorChooseFragment.calcColors();
    }

    @Override
    public List<String> getWordsTextArt() {
        return textAddFragment.getTextAddPresenter().getWords();
    }

    @Override
    public void setMenuItemChecked(int position, boolean isChecked) {
        navigation.getMenu().getItem(position).setChecked(isChecked);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void setViewPagerItem(int itemNumb) {
        viewPager.setCurrentItem(itemNumb);
    }

}

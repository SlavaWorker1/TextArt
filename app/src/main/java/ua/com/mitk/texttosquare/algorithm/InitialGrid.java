package ua.com.mitk.texttosquare.algorithm;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Разбивает битман на сетку прямоугольников,
 * обьеденяет прямоугольники находящиеся на 1ой строке 1 столбце
 */
public class InitialGrid {
    private final String TAG = "INIT_GRID";
    private final String TAG_TIME = "TIME";
    private Bitmap bitmap;
    private List<Rect> splitedRect;
    private int minSquareSize;
    private int[][] pixelsBitmap;


    public InitialGrid(Bitmap bitmap, int minimalSquare) {
        this.bitmap = bitmap;
        this.minSquareSize = minimalSquare;
        splitedRect = new ArrayList<>();
        Log.d(TAG, "BITMAP WIDTH:" + bitmap.getWidth() + "BITMAP_HEIGHT: " + bitmap.getHeight());
        splitedRect = splitRect(new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()));
        makeRectaglesUnion();
    }

    private List<Rect> splitRect(Rect rect) {
        QuadTreeRect quadTree = new QuadTreeRect(rect);
        long time = System.currentTimeMillis();
        getPixelsFromBitmap();
        goInsindeTree(quadTree);
        Log.d(TAG_TIME, "TIME OF SPLEET RECTANGLES " + String.valueOf(System.currentTimeMillis() - time));
        return splitedRect;
    }

    /**
     * Достает пиксели с битмапа и добавляет их в массив
     * конвертирует массив в двумерный
     */
    private void getPixelsFromBitmap() {
        int[] imagePixels = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(imagePixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

        pixelsBitmap = new int[bitmap.getWidth()][bitmap.getHeight()];
        for (int i = 0; i < bitmap.getWidth(); i++)
            for (int j = 0; j < bitmap.getHeight(); j++)
                pixelsBitmap[i][j] = imagePixels[(j * bitmap.getWidth()) + i];
    }


    /**
     * С помощью Дерева квадрантов
     * Находит квадраты заполненные больше 99% и добавляет их в список
     */
    private void goInsindeTree(QuadTreeRect quadTreeRect) {
        if (quadTreeRect.getGraphicRect().width() < minSquareSize) return;
        double occupancyOfRectangle = checkQuadFill(quadTreeRect.getGraphicRect());
        if (occupancyOfRectangle >= 0.99) {
            splitedRect.add(quadTreeRect.getGraphicRect());
        } else {
            if (occupancyOfRectangle >= 0.005 && occupancyOfRectangle < 0.99) {
                goInsindeTree(quadTreeRect.getLeftBottom());
                goInsindeTree(quadTreeRect.getLeftTop());
                goInsindeTree(quadTreeRect.getRightBottom());
                goInsindeTree(quadTreeRect.getRightTop());
            }
        }
    }


    /**
     * @return На сколько процентов заполнен прямоугольник
     */
    private double checkQuadFill(Rect rect) {
        int whitePixels = 0;
        int blackPixels = 0;
        int step = 1;
        for (int i = rect.left; i < rect.right; i += step) {
            for (int j = rect.top; j < rect.bottom; j += step) {
                if (checkPixelAlpha(pixelsBitmap[i][j])) {
                    if (checkPixelColor(pixelsBitmap[i][j])) {
                        blackPixels++;
                    } else {
                        whitePixels++;
                    }
                } else {
                    whitePixels++;
                }
            }
        }
        return ((double) blackPixels) / ((double) (whitePixels + blackPixels));
    }


    /**
     * @return Принадлежит ли пиксель заданному Черному цвету
     */
    private boolean checkPixelColor(int pixel) {
        int MIN_PIXEL_COLOR = 85;
        return (Color.red(pixel) <= MIN_PIXEL_COLOR && Color.green(pixel) <= MIN_PIXEL_COLOR && Color.blue(pixel) <= MIN_PIXEL_COLOR);
    }


    private boolean checkPixelAlpha(int pixel) {
        int alpha = Color.alpha(pixel);
        return alpha != 0;
    }


    private void makeRectaglesUnion() {
        long time = System.currentTimeMillis();
        //Разбиение всего массива на 4 части для ускорения работы и запуск их в потоках
        List<List<Rect>> parts = new ArrayList<>();
        parts.add(new ArrayList<>(splitedRect.subList(0, splitedRect.size() / 4)));
        parts.add(new ArrayList<>(splitedRect.subList(splitedRect.size() / 4, splitedRect.size() / 2)));
        parts.add(new ArrayList<>(splitedRect.subList(splitedRect.size() / 2, 3 * splitedRect.size() / 4)));
        parts.add(new ArrayList<>(splitedRect.subList(3 * splitedRect.size() / 4, splitedRect.size())));

        // Запуск и остановка потоков
        QuadrantsCombinerThread[] quadrantsCombinerThreads = new QuadrantsCombinerThread[4];
        for (int i = 0; i < quadrantsCombinerThreads.length; i++) {
            quadrantsCombinerThreads[i] = new QuadrantsCombinerThread(parts.get(i));
            quadrantsCombinerThreads[i].start();
        }

        try {
            for (QuadrantsCombinerThread quadrantsCombinerThread : quadrantsCombinerThreads) {
                quadrantsCombinerThread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Очищение старого массива и заполнение его новыми обьеденёнными элементами
        splitedRect = new ArrayList<>();
        for (QuadrantsCombinerThread quadrantsCombinerThread : quadrantsCombinerThreads) {
            splitedRect.addAll(quadrantsCombinerThread.getRectsList());
        }

        // Обьединение нового массива
        int unionCount = createHorizontalUnion(splitedRect) + createVerticalUnion(splitedRect);
        while (unionCount > 0) {
            unionCount = createHorizontalUnion(splitedRect) + createVerticalUnion(splitedRect);
        }
        Log.d(TAG_TIME, "TIME OF UNION RECTANGLES " + String.valueOf(System.currentTimeMillis() - time));
    }


    /**
     * Объединяет прямоугольники имеющие одинаковые left и right
     * j < i. Так как bottom = top, а значит элемент находится раньше по списку
     */
    private int createVerticalUnion(List<Rect> rectList) {
        int unionCount = 0;
        for (int i = 0; i < rectList.size(); i++) {
            if (!rectList.get(i).isEmpty()) {
                for (int j = 0; j < i; j++) {
                    if (rectList.get(i).bottom == rectList.get(j).top &&
                            (rectList.get(i).left == rectList.get(j).left &&
                                    rectList.get(i).right == rectList.get(j).right)) {
                        rectList.get(i).bottom = rectList.get(j).bottom;
                        rectList.get(j).setEmpty();
                        unionCount++;
                    }
                }
            }
        }
        return unionCount;
    }

    /**
     * Объединяет прямоугольники имеющие одинаковые top и bottom
     * j = i. Так как right = left, а значит элемент находится впереди по списку
     */
    private int createHorizontalUnion(List<Rect> rectList) {
        int unionCount = 0;
        for (int i = 0; i < rectList.size(); i++) {
            if (!rectList.get(i).isEmpty()) {
                for (int j = i; j < rectList.size(); j++) {
                    if (rectList.get(i).right == rectList.get(j).left &&
                            (rectList.get(i).top == rectList.get(j).top &&
                                    rectList.get(i).bottom == rectList.get(j).bottom)) {
                        rectList.get(i).right = rectList.get(j).right;
                        rectList.get(j).setEmpty();
                        unionCount++;
                    }
                }
            }
        }
        return unionCount;
    }

    public List<Rect> getSplitedRect() {
        return splitedRect;
    }

}

package ua.com.mitk.texttosquare.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;

import ua.com.mitk.texttosquare.BlogViewHolder;
import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.activities.BlogSingleActivity;
import ua.com.mitk.texttosquare.models.Blog;
import ua.com.mitk.texttosquare.presenters.BlogPresenter;
import ua.com.mitk.texttosquare.views.BlogView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogFragment extends Fragment implements BlogView {

    RecyclerView rvBlog;
    private BlogPresenter blogPresenter;

    public BlogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_blog, container, false);
        blogPresenter = new BlogPresenter(this);

        rvBlog = (RecyclerView) view.findViewById(R.id.rvBlog);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rvBlog.setLayoutManager(llm);
        rvBlog.setHasFixedSize(true);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Blog, BlogViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(
                Blog.class,
                R.layout.blog_item,
                BlogViewHolder.class,
                blogPresenter.getmDatabase()
        ) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {
                final String postKey = getRef(position).getKey();

                viewHolder.setImage(model.getImage());
                viewHolder.setUsername(model.getUsername());

                // For recyclerview item click

                // Reference of our views
                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        blogPresenter.singleItemClick(postKey);
                    }
                });

            }
        };
        rvBlog.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public void singleItemIntent(String postKey) {
        Log.v("postKey", "Clicked Post Key is : " + postKey);
        Intent singleBlogItem = new Intent(getActivity(), BlogSingleActivity.class);
        singleBlogItem.putExtra("postId", postKey);
        startActivity(singleBlogItem);
    }
}

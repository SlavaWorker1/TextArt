package ua.com.mitk.texttosquare.adapters;

import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import ua.com.mitk.texttosquare.R;
import ua.com.mitk.texttosquare.utils.ImageDecoder;


public class ImageChooserAdapter extends RecyclerView.Adapter<ImageChooserAdapter.ViewHolder> {

    private List<Integer> drawableList;
    private ImageView imgChoosenTamplate;
    private int chosenID;

    public ImageChooserAdapter(List<Integer> drawableList, ImageView imgChoosenTamplate) {
        this.drawableList = drawableList;
        this.imgChoosenTamplate = imgChoosenTamplate;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_choose_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final int drawableItemId = drawableList.get(position);
        holder.imgTemplate.setImageDrawable(ContextCompat.getDrawable(
                imgChoosenTamplate.getContext(),
                drawableItemId
        ));

        try {
            holder.imgTemplate.post(new Runnable() {
                @Override
                public void run() {
                    Bitmap bm = ImageDecoder.decodeSampledBitmapFromDrawable(
                            holder.imgTemplate.getContext(),
                            drawableItemId,
                            holder.imgTemplate.getWidth(),
                            holder.imgTemplate.getHeight()
                    );
                    holder.imgTemplate.setImageBitmap(bm);
                }
            });
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgChoosenTamplate.setImageDrawable(ContextCompat.getDrawable(
                        imgChoosenTamplate.getContext(),
                        drawableItemId
                ));
                chosenID = drawableItemId;
            }
        });
    }

    @Override
    public int getItemCount() {
        return drawableList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgTemplate;
        private View itemView;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imgTemplate = (ImageView) itemView.findViewById(R.id.imgTemplate);
        }
    }

    public int getDrawableId() {
        return chosenID;
    }
}

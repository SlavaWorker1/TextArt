package ua.com.mitk.texttosquare.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ImageDecoder {
    public static Bitmap decodeSampledBitmapFromDrawable(Context activity, int res, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(activity.getResources(), res, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inScaled = false;
        return BitmapFactory.decodeResource(activity.getResources(), res, options);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static String writeBitmapToFile(Context context, Bitmap bitmap) {
        String extension = ".jpg";
        String name = "TextART" + extension;
        FileOutputStream fos = null;
        String userDir = getCacheDir(context);
        try {
            File dir = new File(userDir);
            dir.mkdir();
            fos = new FileOutputStream(userDir + "/" + name);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return userDir + "/" + name;
    }

    public static String getCacheDir(Context context) {
        String state = Environment.getExternalStorageState();
        File filesDir;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            filesDir = context.getExternalCacheDir();
            if (filesDir == null){
                filesDir = context.getFilesDir();
            }
        } else {
            filesDir = context.getFilesDir();
        }
        return filesDir.getAbsolutePath();
    }
}
